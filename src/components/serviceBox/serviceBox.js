import React, {Component} from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,

} from 'react-router-dom';
import './serviceBox.css';
import Homepage from "../hompage";


class Servicebox extends Component {
    constructor(props) {

        // Required to call original constructor
        super(props);

        // Props are now accessible from here
        // var v = props.reactProp;
    }
    render() {
    let boxHeadline = this.props.headline;
    let buttonLink = '?' + boxHeadline.toLowerCase().replace(' ','_');
    let serviceContent1 = this.props.serviceContent1;
    let serviceContent2 = this.props.serviceContent2;
    let serviceContent3 = this.props.serviceContent3;
    let serviceContent4 = this.props.serviceContent4;
    let serviceContent5 = this.props.serviceContent5;
    let inclusiveContent = this.props.inclusiveContent;
    let boxPrice = this.props.price;

    // handleContactSubject = (e) => {
    // };

        return(
            <div className="service-card">
                <div className="service-card-body">
                    <h4 className="service-card-title">{boxHeadline}</h4>
                    <div className="service-card-content">
                        <div className="service-content">
                            <h5>Leistungen:</h5>
                            <li className="service-card-text">{serviceContent1}</li>
                            <li className="service-card-text">{serviceContent2}</li>
                            <li className="service-card-text">{serviceContent3}</li>
                            <li className="service-card-text">{serviceContent4}</li>
                            <li className="service-card-text">{serviceContent5}</li>
                        </div>
                        <div class="seperator-container"></div>
                        <div className="inclusive-content">
                            <h5>Inklusive:</h5>
                            <p className="service-card-text">{inclusiveContent}</p>
                        </div>
                    </div>
                    <p className="card-price">{boxPrice}</p>
                    <Router>
                        <Link to={buttonLink} className="service-btn btn btn-primary">Leistung anfragen</Link>
                    </Router>
                </div>
            </div>
        );
    }
};

export default Servicebox;