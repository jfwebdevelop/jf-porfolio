import React, { Component } from 'react';

import Landing from './sections/landing';
import Service from "./sections/service";
import Reference from "./sections/reference";
import Skills from "./sections/skills";
import About from "./sections/about";
import Contact from "./sections/contact";
import Parallax from "./sections/parallax";
//components
import Toolbar from "../components/Toolbar/toolbar";
import SideDrawer from "../components/SideDrawer/SideDrawer";
import Backdrop from "../components/Backdrop/Backdrop";




class Homepage extends Component {

    constructor(props) {
        super(props);
        // this.serviceHeadline = React.createRef();

        this.state = {
            serviceName : ''
        }
    }
    state = {
        sideDrawerOpen: false,
    };

    drawerToggleClickHandler = () => {
        // this.setState((prevState) => {
        //     return {sideDrawerOpen: !prevState.sideDrawerOpen};
        // });
        if(this.state.sideDrawerOpen) {
            this.setState({sideDrawerOpen: false});
        } else {
            this.setState({sideDrawerOpen: true});
        }
    };

    backdropClickHandler = () => {
        this.setState({sideDrawerOpen: false});
    };

    drawerToggleButtonClickHandler = () => {
        this.setState({sideDrawerOpen: false});
    };

    getServiceName = (val) => {
        this.setState({
            serviceName: val
        });
    };

    render() {
        let sideDrawer;
        let backdrop;
        let drawerButton;
        if(this.state.sideDrawerOpen) {
            backdrop = <Backdrop click={this.backdropClickHandler}/>;
        }
        // console.log(window.location.search);
        return (
            <div className="">
                <Toolbar show={this.state.sideDrawerOpen} drawerClickHandler={this.drawerToggleClickHandler} />
                <SideDrawer  show={this.state.sideDrawerOpen}/>
                {backdrop}
                <Landing/>
                <Service serviceHeadline={this.getServiceName} />
                <Parallax/>
                <Reference/>
                <Skills/>
                <About/>
                <Contact serviceName={this.state.serviceName}  />

            </div>
        );
    }
}

export default Homepage;
