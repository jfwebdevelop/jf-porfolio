import React, { Component } from 'react';

class notFound extends Component {
    render() {
        return (
            <div id="notFound">
                <div className="container-fluid">
                    Page not found!
                </div>
            </div>
        );
    }
}

export default notFound;
