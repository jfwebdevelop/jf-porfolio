import React, {Component} from 'react';
import GoogleMapReact from 'google-map-react';

class SimpleMap extends Component {
    static defaultProps = {
        center: {
            lat: 53.5549816,
            lng: 10.0789467
        },
        zoom: 14
    };

    render() {
        const apiKey = 'AIzaSyBXmKgC_7Tt5GAhB1MN8UG7XcxV6L980LI';
        return (
            // Important! Always set the container height explicitly
            <div style={{ height: '40vh', width: '100%' }}>
                <GoogleMapReact
                    bootstrapURLKeys={{ key: apiKey }}
                    defaultCenter={this.props.center}
                    defaultZoom={this.props.zoom}
                >

                </GoogleMapReact>
            </div>
        );
    }
}

export default SimpleMap;