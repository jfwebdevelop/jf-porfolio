import React, { Component } from 'react';
import {
    Link
} from 'react-router-dom';

class Footer extends Component {
    render() {
        return (
                <footer>
                    <ul>
                        <li>
                            <Link to="/impressum">Impressum</Link>
                        </li>
                        <li>
                            <Link to="/datenschutz">Datenschutz</Link>
                        </li>
                    </ul>
                </footer>
        );
    }
}

export default Footer;
