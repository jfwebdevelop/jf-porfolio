import React, { Component } from 'react';

class About extends Component {
    render() {
        return (
            <section id="about" className="hideme">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-5 image">
                            <img src="https://www.wwweb-media.de/assets/images/cap.png" alt="cap" />
                        </div>
                        <div className="col-md-6 info">
                            <div className="facts col-md-12 row">
                                <div className="years col-md-4"><span>22</span>Jahre alt</div>
                                <div className="job col-md-4"><span>2</span>Jahre Agenturerfahrung</div>
                                <div className="education col-md-4"><span>3,5</span>Jahre Studium</div>
                            </div>
                            <div className="bio">
                                Nach meinem Abitur 2015 bin ich nach Hamburggezogen, um hier <span className="text-quote">Webdesign & Development</span> zu studieren.Neben meinem Studium habe ich bereits in meinerHochschule als Supervisor und Dozent gearbeitet undim März 2017 bei einer Web Agentur neben meinem Studium angefangen zu arbeiten.Hier habe ich individuelle Lösungen für Wettanbieter-Vegleichsseiten erstellt.
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default About;
