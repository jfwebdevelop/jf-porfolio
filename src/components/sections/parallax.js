import React, { Component } from 'react';

class Parallax extends Component {
    render() {
        return (
            <section id="parallax">
                <div className="parallax"></div>
            </section>
        );
    }
}

export default Parallax;
