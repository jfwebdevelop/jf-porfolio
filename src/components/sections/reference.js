import React, { Component } from 'react';

class Reference extends Component {
    render() {
        return (
            <section id="reference" className="hideme">
                <div className="img-container text-center">
                    <img src="https://www.wwweb-media.de/assets/images/projects.png" alt="Portfolio Projects" />
                </div>
            </section>
        );
    }
}

export default Reference;
