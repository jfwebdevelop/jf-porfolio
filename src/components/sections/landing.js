import React, { Component } from 'react';
import Mainbutton from "../mainButton/mainButton";

class Landing extends Component {
    render() {
        return (
            <section id="landing">
                <h1><img src="https://www.wwweb-media.de/assets/images/jf-logo.png" alt="JF Logo" /></h1>
                <div className="container-fluid">
                    <div className="headline-container">
                        <div className="vertical-line"></div>
                        <div className="headlines">
                            <h2 className="typewriter">Webdesign</h2>
                            <h2 className="typewriter">Webdevelopment</h2>
                        </div>
                    </div>
                    <Mainbutton text="Kontakt aufnehmen" />
                </div>
            </section>
        );
    }
}

export default Landing;
