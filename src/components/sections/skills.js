import React, { Component } from 'react';

class Skills extends Component {
    render() {
        return (
            <section id="skills" className="col-md-12 hideme">
                <div className="container-fluid">
                    <div className="card col-md-12 col-lg-3">
                        <img className="card-img-top" src="https://www.wwweb-media.de/assets/images/design-icon.png" alt="Design Icon"/>
                            <div className="card-body">
                                <h4 className="card-title">individuelles Design</h4>
                                <p className="card-text">Ich entwerfe für Sieein auf Ihre Wünscheabgestimmtes Designund berate Sie gernebei Ihren Wünschenund Anregungen.</p>
                                <a href="#contact" className="btn btn-primary">Kontakt aufnehmen</a>
                            </div>
                    </div>
                    <div className="card col-md-12 col-lg-3">
                        <img className="card-img-top" src="https://www.wwweb-media.de/assets/images/project_management-icon.png" alt="Project Management Icon"/>
                        <div className="card-body">
                            <h4 className="card-title">angepasste Lösungen</h4>
                            <p className="card-text">Ich entwickel IhreVorstellungen und berateSie gerne aus Sicht einesWebentwicklers. Mein Zielist die perfekte UmsetzungIhrer Wünsche..</p>
                            <a href="#contact" className="btn btn-primary">Kontakt aufnehmen</a>
                        </div>
                    </div>
                    <div className="card col-md-12 col-lg-3">
                        <img className="card-img-top" src="https://www.wwweb-media.de/assets/images/cms-icon.png" alt="CMS Icon"/>
                        <div className="card-body">
                            <h4 className="card-title">Content Management</h4>
                            <p className="card-text">Ich setze für Sieein Content ManagementSystem um, das aufIhre Wünsche abgestimmtist. So können Sieihre Inhalte verwalten.</p>
                            <a href="#contact" className="btn btn-primary">Kontakt aufnehmen</a>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default Skills;
