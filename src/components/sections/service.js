import React, { Component } from 'react';
import Servicebox from "../serviceBox/serviceBox";
import {BrowserRouter as Router, Link} from "react-router-dom";

let data = [
    {
        "id": 1,
        "headline": "Webpaket Einfach",
        "offer1": "statische Website",
        "offer2": "1 Seite (HTML)",
        "offer3": "Design (maßgefertigt)",
        "offer4": "HTML5 Template (maßgefertigt)",
        "offer5": "-",
        "inclusive": "Beratung + Einrichtung",
        "price": "550€*"
    },
    {
        "id": 2,
        "headline": "Webpaket Advanced",
        "offer1": "statische Website",
        "offer2": "3 Seiten (HTML)",
        "offer3": "Design (maßgefertigt)",
        "offer4": "HTML5 Template (maßgefertigt)",
        "offer5": "-",
        "inclusive": "Beratung + Einrichtung",
        "price": "1250€*"
    },
    {
        "id": 3,
        "headline": "Webpaket Premium",
        "offer1": "statische Website",
        "offer2": "bis zu 10 Unterseiten (HTML)",
        "offer3": "1 Designentwurf",
        "offer4": "HTML5 Template (individuell)",
        "offer5": "Social Media Einbindung",
        "inclusive": "Beratung + Einrichtung",
        "price": "2600€*"
    },
    {
        "id": 4,
        "headline": "CMS Basic",
        "offer1": "dynamische Wordpress Website",
        "offer2": "bis zu 5 Unterseiten",
        "offer3": "Design von Theme abhängig",
        "offer4": "vorgefertigtes Theme",
        "offer5": "-",
        "inclusive": "Beratung + Einrichtung",
        "price": "2200€*"
    },
    {
        "id": 5,
        "headline": "CMS Advanced",
        "offer1": "dynamische Wordpress Website",
        "offer2": "bis zu 15 Unterseiten",
        "offer3": "2 Designentwürfe",
        "offer4": "individuelles Theme",
        "offer5": "-",
        "inclusive": "Beratung + Einrichtung",
        "price": "3100€*"
    },
    {
        "id": 6,
        "headline": "CMS Premium",
        "offer1": "dynamische Wordpress Website",
        "offer2": "bis zu 30 Unterseiten",
        "offer3": "3 Designentwürfe",
        "offer4": "individuelles Theme",
        "offer5": "Login / Registrierung",
        "inclusive": "Beratung + Einrichtung",
        "price": "4500€*"
    },
    {
        "id": 7,
        "headline": "Webapp Basic",
        "offer1": "moderne Webanwendung",
        "offer2": "3 Seiten",
        "offer3": "1 Designentwurf",
        "offer4": "Kontaktformular",
        "offer5": "-",
        "inclusive": "Beratung + Einrichtung",
        "price": "2100€*"
    },
    {
        "id": 7,
        "headline": "Webapp Business",
        "offer1": "moderne Webanwendung",
        "offer2": "bis zu 10 Seiten",
        "offer3": "2 Designentwürfe",
        "offer4": "Login/Registrierung",
        "offer5": "Kontaktformular & Newsletter",
        "inclusive": "Beratung + Einrichtung",
        "price": "4200€*"
    },
    {
        "id": 8,
        "headline": "Individuelles Paket",
        "offer1": "In einem kostenlosen Gespräch",
        "offer2": "entscheiden wir gemeinsam,",
        "offer3": "was das Beste für Sie ist",
        "offer4": "",
        "offer5": "",
        "inclusive": "Die Beratung ist kostenlos",
        "price": "ab 0€"
    },
];

class Service extends Component {
    // state = {
    //   clickCounter: 0
    // };
    constructor(props) {
        super(props);

        this.state = {shared_var: "init"};


        this.state = {
            items: [],
            visible: 3,
            error: false,
            serviceName: ''
        };

        this.loadMore = this.loadMore.bind(this);
    }

    loadMore() {
        this.setState((prev) => {
            return {visible: prev.visible + 3};
        });
    }



    componentDidMount() {
        this.setState({
            items: data
        });
    }

    render() {
        // console.log(this.props.params.id);
        return (
            <section id="service" className="hideme">
                <div className="container-fluid" aria-live="polite">
                    {this.state.items.slice(0, this.state.visible).map((item, index) => {
                        return (
                            <div className="service-card" key={index}>
                                <div className="service-card-body">
                                    <h4 className="service-card-title">{item.headline}</h4>
                                    <div className="service-card-content">
                                        <div className="service-content">
                                            <h5>Leistungen:</h5>
                                            <li className="service-card-text">{item.offer1}</li>
                                            <li className="service-card-text">{item.offer2}</li>
                                            <li className="service-card-text">{item.offer3}</li>
                                            <li className="service-card-text">{item.offer4}</li>
                                            <li className="service-card-text">{item.offer5}</li>
                                        </div>
                                        <div className="seperator-container"></div>
                                        <div className="inclusive-content">
                                            <h5>Inklusive:</h5>
                                            <p className="service-card-text">{item.inclusive}</p>
                                        </div>
                                    </div>
                                    <p className="card-price">{item.price}</p>
                                    <Router>
                                        <a href='#contact' onClick={() => { this.props.serviceHeadline(item.headline) }} className="service-btn btn btn-primary">Leistung
                                            anfragen</a>
                                    </Router>
                                </div>
                            </div>
                        );
                    })}
                </div>
                {this.state.visible < this.state.items.length &&
                <button onClick={this.loadMore}  type="button" className="load-more">mehr laden <span></span><i className="fas fa-plus"></i></button>
                }
                <p>* Preis kann ggf. durch zusätzliche Wünsche erhöht werden (transparent) </p>
                                        {/*<button onClick={this.props.greet} className="btn btn-primary">Greet</button>*/}
            </section>
        );
    }
}

// Service.propTypes = {
//     greet: React.PropTypes.func
// };

export default Service;
