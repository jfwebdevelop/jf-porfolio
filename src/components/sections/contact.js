import React, { Component } from 'react';
import axios from 'axios';
import FormErrors from '../FormErrors/FormErrors';
import SimpleMap from '../Map/map';


class Contact extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            subject: '',
            email: '',
            nachricht: '',
            formErrors: {name: '', email: '', nachricht: ''},
            nameValid: false,
            emailValid: false,
            nachrichtValid: false,
            checkBoxValid: false,
            formValid: false,
            checkBox: '',
            params: '',
            feedback: '',
            serverFeedbackPos: '',
            serverFeedbackNeg: '',
            messageSent: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = e => {
      this.setState({[e.target.name]: e.target.value})
    };

    handleUserInput = e => {
      const name = e.target.name;
      const value = e.target.value;
      this.setState({[name]: value},
          () => {this.validateField(name,value)});
    };

    handleCheckboxInput = e => {
      const checked = e.target.checked;
      this.setState(() => ({
          checkBoxValid: checked
      })
      );
    };

    validateField(fieldName, value){
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let nameValid = this.state.nameValid;
        let nachrichtValid = this.state.nachrichtValid;
        // let checkboxValid = this.state.checkBoxValid;

        switch(fieldName) {
            case 'name':
                nameValid = value.length > 1;
                fieldValidationErrors.name = nameValid ? '' : ' ist zu kurz';
                break;
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : ' ist ungültig';
                break;
            case 'nachricht':
                nachrichtValid = value.length > 1;
                fieldValidationErrors.nachricht = nachrichtValid ? '' : ' ist zu kurz';
                break;
            default:
                break;
        }

        this.setState({formErrors: fieldValidationErrors,
            nameValid: nameValid,
            emailValid: emailValid,
            nachrichtValid: nachrichtValid,

        }, this.validateForm);

    }

    validateForm() {
        this.setState({formValid: this.state.nameValid && this.state.emailValid && this.state.nachrichtValid});
    }

    // componentDidUpdate(prevProps, prevState, snapshot) {
    //     if(this.state.subject === '') {
    //         this.setState({subject: this.props.serviceName})
    //     }
    // }

    async handleSubmit(e) {
      e.preventDefault();

      // if(this.props.serviceName !== '') {
      //
      // }
      const {name, nachricht, email} = this.state;


      // if(this.state.subject === '') {
      //     subject = this.props.serviceName;
      // } else {
      //     subject = this.state.subject;
      // }
      let subject = this.refs.subject_input.value;
      if(subject === '') {
        subject = this.state.subject;
      }

      await axios.post('/api/form', {
            name,
            subject,
            nachricht,
            email
        }).then(res => {
            //TODO: Abfragen ob value vom Input leer ist --> Input Value löschen, ansonsten defaultValue nehmen
            console.log(this.refs.subject_input.value);
            // console.log(this.refs.subject_input.defaultValue);
            let defaultSubjectValue = this.refs.subject_input.defaultValue;
            // if(defaultSubjectValue === '') {
            //     this.refs.subject_input.value = '';
            // } else {
            //     this.refs.subject_input.defaultValue = '';
            //     this.refs.subject_input.value = '';
            // }

          this.setState({serverFeedbackPos: <p className='alert alert-success' id='success-alert'>Vielen Dank für Ihre Nachricht. Ich werde mich schnellstmöglich um die Bearbeitung kümmern.</p>});
            this.setState({
                // name: '',
                // subject: '',
                // email: '',
                // nachricht: '',
                // feedback: 'Nachricht abgeschickt',
                messageSent: true
            });

        }).catch(error => {
            console.log(error);
          this.setState({serverFeedbackNeg: 'Es ist ein unerwarteter Fehler aufgetreten'});
        })

    };


    render() {
        if(!this.state.messageSent) {
            return (
                <section id="contact" className="hideme">
                    <div className="container-fluid">
                        <FormErrors formErrors={this.state.formErrors} />
                        {this.state.serverFeedbackPos}
                        <p>{this.state.serverFeedbackNeg}</p>
                        <form onSubmit={this.handleSubmit}>
                            <div className="email-input row justify-content-md-center">
                                <input
                                    className='col-md-3'
                                    name='name'
                                    placeholder='Name*'
                                    value={this.state.name}
                                    onChange={(event) => this.handleUserInput(event) && this.handleChange}
                                />
                                <input
                                    className='col-md-3'
                                    name='subject'
                                    ref='subject_input'
                                    placeholder='Betreff'
                                    defaultValue={this.props.serviceName}
                                    onChange={this.handleChange}
                                />
                                <input
                                    className='col-md-3'
                                    type='email'
                                    name='email'
                                    placeholder='E-Mail*'
                                    value={this.state.email}
                                    onChange={(event) => this.handleUserInput(event) && this.handleChange}
                                />
                            </div>
                            <div className="email-input row">
                                <textarea
                                    className='col-md-6'
                                    name='nachricht'
                                    placeholder='Ihre Nachricht*'
                                    value={this.state.nachricht}
                                    onChange={(event) => this.handleUserInput(event) && this.handleChange}
                                >Meine Nachricht</textarea>
                            </div>
                            <div className="form-check">
                                <input
                                    type="checkbox"
                                    name="checkBox"
                                    className="form-check-input"
                                    id="exampleCheck1"
                                    onChange={(event) => this.handleCheckboxInput(event) && this.handleChange}
                                />
                                <label className="form-check-label" htmlFor="exampleCheck1">Ich stimme der <a href="/datenschutz">Datenschutzerklärung</a> zu</label>
                            </div>
                                <button disabled={!this.state.checkBoxValid || !this.state.formValid} onClick={e => this.handleSubmit(e)}>senden<i className="fas fa-paper-plane"></i></button>
                        </form>
                    </div>
                    <SimpleMap />
                </section>
            );
        } else {
            return(
                <section id="contact" className="hideme">
                    <div className="container-fluid">
                        <FormErrors formErrors={this.state.formErrors} />
                        {this.state.serverFeedbackPos}
                    </div>
                    <SimpleMap />
                </section>
            );
        }
    }
}

export default Contact;
