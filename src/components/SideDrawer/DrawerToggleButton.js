import React from 'react';

import './DrawerToggleButton.css'

const DrawerToggleButton = props => {

    let buttonClasses = 'toggle-button';
    if(props.show) {
        buttonClasses = 'toggle-button open';
    }

    return(
    <button className={buttonClasses} onClick={props.click}>
        <div className="toggle-button__line one"></div>
        <div className="toggle-button__line two"></div>
        <div className="toggle-button__line three"></div>
    </button>
    );
};

export default DrawerToggleButton;