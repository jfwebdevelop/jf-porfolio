import React from 'react';

import './DrawerToggleButton';
import './SideDrawer.css';

const sideDrawer = props => {

    let drawerClasses = 'side-drawer';
    if(props.show) {
        drawerClasses = 'side-drawer open';
    }

    return(
        <nav className={drawerClasses}>
            <ul>
                <li><a href="#landing">Home <i className="fas fa-home"></i></a></li>
                <li><a href="#service">Leistungen<i className="fas fa-code"></i></a></li>
                <li><a href="#reference">Referenzen<i className="fas fa-globe-europe"></i></a></li>
                <li><a href="#skills">Fähigkeiten<i className="fas fa-sign-language"></i></a></li>
                <li><a href="#about">Über mich<i className="fas fa-user-tie"></i></a></li>
                <li><a href="#contact">Kontakt<i className="far fa-comments"></i></a></li>
            </ul>
        </nav>
    );
};

export default sideDrawer;