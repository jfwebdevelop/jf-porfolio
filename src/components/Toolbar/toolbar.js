import React from 'react';


import DrawerToggleButton from "../SideDrawer/DrawerToggleButton";
import '../SideDrawer/DrawerToggleButton';
import './toolbar.css';
const Toolbar = props => (
  <header className="toolbar">
      <nav className="toolbar__navigation">
          {/*<div className="toolbar__logo"><a href="/">THE LOGO</a></div>*/}
          <div className="spacer"></div>
          <div>
              <DrawerToggleButton show={props.show} click={props.drawerClickHandler}/>
          </div>
      </nav>
  </header>
);

export default Toolbar;