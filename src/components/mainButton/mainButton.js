import React from 'react';

import './mainButton.css';

const Mainbutton = props => {
    let buttonText = props.text;

    return(
        <div className="button-container">
            <a href="#contact">
                {buttonText}
            </a>
        </div>
    );
};

export default Mainbutton;