import React from 'react';

const FormErrors = ({formErrors}) =>
    <div className='formErrors'>
        {Object.keys(formErrors).map((fieldName, i) => {
            if(formErrors[fieldName].length > 0){
                return(
                    <div className='col-md-6 alert-container' key={i}>
                        <p key={i}><i className="fas fa-info-circle"></i>{fieldName.charAt(0).toUpperCase() + fieldName.slice(1)} {formErrors[fieldName]}</p>
                    </div>
                )
            } else{
                return '';
            }
        })}
    </div>;

export default FormErrors;