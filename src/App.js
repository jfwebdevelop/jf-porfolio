import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route

} from 'react-router-dom';

//components
import Backdrop from "./components/Backdrop/Backdrop";
import Footer from "./components/footer/footer";
import Homepage from "./components/hompage";
import Impressum from "./components/impressum/impressum";
import Datenschutz from "./components/Datenschutz/datenschutz";

class App extends Component {

    state = {
      sideDrawerOpen: false
    };

    drawerToggleClickHandler = () => {
        if(this.state.sideDrawerOpen) {
            this.setState({sideDrawerOpen: false});
        } else {
            this.setState({sideDrawerOpen: true});
        }
    };

    backdropClickHandler = () => {
        this.setState({sideDrawerOpen: false});
    };

    drawerToggleButtonClickHandler = () => {
        this.setState({sideDrawerOpen: false});
    };


  render() {
      if(this.state.sideDrawerOpen) {
          let backdrop = <Backdrop click={this.backdropClickHandler}/>;
      }
    return (
        <Router>
          <div className="mainApp" style={{height: '100%'}}>
              {/*<Toolbar show={this.state.sideDrawerOpen} drawerClickHandler={this.drawerToggleClickHandler} />*/}
              {/*<SideDrawer  show={this.state.sideDrawerOpen}/>*/}
              {/*{backdrop}*/}
                  <main>
                    <Route exact path='/' component={Homepage} />
                    <Route path='/impressum' component={Impressum} />
                    <Route path='/datenschutz' component={Datenschutz} />
                  </main>
            <Footer/>
          </div>
        </Router>
    );
  }
}

export default App;
